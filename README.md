# Lentil Translate Android
 An un offcial Android client for the Lingva Translate
 
Available on the Google Play Store:
https://play.google.com/store/apps/details?id=dev.atajan.lingva_android&hl=en_US&gl=US

For more information about Lingva Translate:
https://github.com/TheDavidDelta/lingva-translate

To try ou the web client:
https://lingva.ml

See the upcoming changes on the public roadmap:
https://github.com/users/yaxarat/projects/1

## Supports Multiple Themes, Including Matrial You
Lentil Dark | Lentil Light
:-------------------------:|:-------------------------:
![](https://user-images.githubusercontent.com/27980758/149836952-2b3ed4f3-a29f-416b-bf78-add98508e701.png)  |  ![](https://user-images.githubusercontent.com/27980758/149836964-1b7d4060-b429-44b0-b88f-b8d0f042b4d8.png)

Fancy App Theme to Match | Your Fancy Wallpaper
:-------------------------:|:-------------------------:
![](https://user-images.githubusercontent.com/27980758/149836993-2e7d8f7d-9658-4c9c-ad1e-69b451796292.png)  |  ![](https://user-images.githubusercontent.com/27980758/149837022-255b97ef-5b7b-475e-abe2-d63a7ecdc8c0.png)
